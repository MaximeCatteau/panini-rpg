import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit{
  rows: Number = 10;
  columns: Number = 10;
  constructor() { }

  ngOnInit() {
  }

  /**
   * Return to array the value of rows or columns for map grid
   * @param value Number 
   */
  numberToMap(value: Number) {
    return new Array(value);
  }

  /**
   * Debug mode, display on console log the current target position of cell
   * @param rows Number
   * @param columns Number
   */
  celluleReturn(rows, columns){
    console.log('Cellule : ', rows, ',' , columns);
  }
}